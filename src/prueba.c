#include "slaballoc.h"
#include "objetos.h"
#define EN_USO 1
#define DISPONIBLE 0

void imprimirdatos(int k,SlabAlloc* alloc,Slab* s){
	printf("////////\tSlabAlloc: %s\t Cantidad en Uso: %d\n///////",alloc->nombre, alloc->cantidad_en_uso);
	for(int n=0;n<k;n++){
		printf("Objeto %i, esta: ",n);
		if((*(s+n)).status==1) printf("EN_USO\n");
		else printf("DISPONIBLE\n");
		printf("Direccion mem_ptr %d: %p̣\t",n,alloc->mem_ptr+n*sizeof(Ejemplo));
		printf("Direccion slab %d: %p̣\n",n,s->ptr_data+n*sizeof(Ejemplo));
		printf("Direccion slab old %d: %p̣\n",n,s->ptr_data_old+n*sizeof(Ejemplo));
		
		

	}	

}

int main(){
	SlabAlloc* alloc =  crear_cache("SlabAllocator de Ejemplo", sizeof(Ejemplo), crear_Ejemplo, destruir_Ejemplo);
	
	//Slab* s = alloc->slab_ptr;
		
	SlabAlloc* alls =  crear_cache("SlabAllocator 2", sizeof(Ejemplo), crear_Ejemplo, destruir_Ejemplo);
	
	//Slab* sss = alls->slab_ptr;

	for(int n=0;n<250;n++){
		obtener_cache(alls, 0);
	}
	obtener_cache(alls, 1);
	obtener_cache(alls, 1);
	obtener_cache(alls, 1);
	obtener_cache(alls, 1);
	obtener_cache(alls, 1);
	obtener_cache(alls, 1);
	stats_cache(alls);

	
	obtener_cache(alloc, 0);
	obtener_cache(alloc, 0);
	obtener_cache(alloc, 0);
	obtener_cache(alloc, 0);
	
	obtener_cache(alloc, 0);
	obtener_cache(alloc, 0);
	obtener_cache(alloc, 0);
	obtener_cache(alloc, 1);
	void* pt=obtener_cache(alloc, 0);
	devolver_cache(alloc, pt);
	obtener_cache(alloc, 0);
	obtener_cache(alloc, 0);
	obtener_cache(alloc, 0);
	stats_cache(alloc);
	//imprimirdatos(10,alloc,s);
	
	destruir_cache(alloc);

	return 0;


}





